<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storeGuest(Request $request)
{
    // Validate the guest order input
    $validatedData = $request->validate([
        'email' => 'required|email',
        // Add more validation rules for the required information
    ]);

    // Create a new order for the guest
    $order = new Order;
    $order->email = $request->input('email');
    // Set other order properties as needed
    $order->save();

    // Redirect or show a success message to the guest
    return redirect()->back()->with('success', 'Order placed successfully');
}

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
