<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $route = 'admin/users';
    public $rootFolder = 'admin.users';

    public function index()
    {
        //

        $records = User::get();
        return response()->json([
            'success' => true,
            'desctiption' => 'Data ertrived successfully',
            'data' => $records
        ]);

        return view($this->rootFolder.".index");

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view($this->rootFolder. ".create");

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {



        $record = User;
        $record->name = $request->name;
        $record->email = $request->email;
        $record->phone = $request->phone;
        $record->status = 1;
        $record->password = Hash::make($request->password);
        $record->user_type = $request->user_type; //Administrator(1) Or Guest(2);
        $record->save();

        return back();


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //

        $data['record'] = User::findOrFail($id);
        return view($this->rootFolder. ".show". $data);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $data['record'] = User::findOrFail($id);
        return view($this->rootFolder. ".edit". $data);
        return back();

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $record = User::findOrFail($id);
        $record->name = $request->name;
        $record->email = $request->email;
        $record->phone = $request->phone;
        if(isset($request->password))
        {
            $record->password = Hash::make($request->password);
        }
        $record->save();

        return back();

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $record = User::findOrFail($id)->delete;

        return back();

    }
}