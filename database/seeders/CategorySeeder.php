<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         // Create categories

         Category::truncate();

         $categories = [
               ["name" => "Electronics","id" =>"1"],
               ["name" => "Fashion and Apparel","id" =>"2"],
               ["name" => "Home and Kitchen","id" =>"3"],
               ["name" => "Beauty and Personal Care","id" =>"4"],
               ["name" => "Health and Fitness","id" =>"5"],
               ["name" => "Baby and Kids","id" =>"6"],
               ["name" => "Books, Music, and Entertainment","id" =>"7"],
               ["name" => "Automotive and Tools","id" =>"8"],
               ["name" => "Sports and Outdoors","id" =>"9"],
               ["name" => "Groceries and Food","id" =>"10"],
         ];
         foreach ($categories as $key => $data) {
             Category::create($data);
         }
    }
}
