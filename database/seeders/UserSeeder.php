<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Database\Seeder\UserType;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User;
        $user->name = 'Admin';
        $user->email = 'admin@example.com';
        $user->phone = '91941553';
        $user->password = Hash::make('12345678');
        $user->user_type = 1;
        $user->save();


        $user = new User;
        $user->name = 'Guest';
        $user->email = 'guest@example.com';
        $user->phone = '91941553';
        $user->password = Hash::make('12345678');
        $user->user_type = 2;
        $user->save();

    }
}
